NF: cantidad de campos en un AWK


FS: separado de archivo de entrada
$0: el registro completo
export $LC_TIME=es_CL.utf8 : fecha en español
stty -istrip cs8 : añade Ñ al juego de caracteres de unix

establecer fecha y hora en español











################
#SED
################

#Obtener linea numero 2 de un archivo
sed -n 2p archivo.txt 




#Elimina ultima fila de un archivo 
sed '$d' archivo.txt


#Obtener lineas de la 5 a la 7 de un archivo
sed -n 5,7p archivo.txt 


#Busca palabra y elimina fila correspondiente
sed '/palabra/d' archivo.txt


#Eliminar Primeras dos lineas de un archivo
sed '1,2d' archivo.txt 

#Eliminar primera y ultima fila de un archivo
sed '1d' CARDET_CIE.TXT | sed '$d'

#Eliminar filas vacias
sed '/./!d' archivo.txt 


#inserta un correlativo al inicio de cada fila
sed = archivo.txt | sed 'N;s/\n/\ /'

#inserta caracter al final de cada fila ( en este caso, inserta A)
sed 's/.$/&A/g' archivo.txt


#remplaza un caracter por otro (ej: espacio por vacio)
sed -e 's/[ ]//g' archivo.txt


#Para añadir una cadena a cada linea de un fichero
cat archivo.txt | sed 's/^/TEXTO/'


# reemplaza . por vacio en una variable
variable=16.123.465.15
echo $variable | sed -e 's/[.]//g' 

#Eliminar filas repetidas consecutivamente
sed '$!N; /^\(.*\)\n\1$/!P; D' archivo.txt

#Eliminar filas repetidas no consecutivas
sed -n 'G; s/\n/&&/; /^\([ -~]*\n\).*\n\1/d; s/\n//; h; P'

#Elimina ultimo caracter (remplaza utlitmo caracter por nada)
echo $CADENA | sed 's/.$//g'

#Elimina espacios y tabulaciones al final de una linea
sed 's/[ \t]*$//' archivo.txt

#guarda modificaciones en el fichero original y guarda un respaldo de la informacion anterior en un fichero .bkp
sed -i".bak" '3d' mi_fichero.txt

################
#AWK
################





#para un archivo con separador '	' para una columna que coinsida con 'palabra' obtiene un campo determinado 
variable=`awk 'BEGIN {FS="	"}; $1 ~/'palabra'/ {print $2}' archivo.txt`

#Obtener el valor de un registro que no tiene separador
awk '{print substr($0,2,5)}' archivo.txt






#Obtener la columna 2 y 4 de un archivo con separador
awk 'BEGIN {FS="|"}; {print $2 $4}' archivo.txt



#Asignar un valor del archivo a una varible. Esto cuando sea un registro del archivo.
Fecha=`awk 'BEGIN {FS="|"}; { print $2}' archivo.txt`


#Uso de Substring
awk 'BEGIN {FS="|"}; { substr($3,1,5)}' archivo.txt


#Uso de if
awk 'BEGIN {FS="|"}; { if (substr($1,3,1) == "-") print $0 }' archivo.txt




#Uso de if con And
awk 'BEGIN {FS="|"}; { if (substr($1,3,1) != "C" && $5 == "PME" ) print $0 }' archivo.txt


#Uso de if con Or
awk 'BEGIN {FS="|"}; { if (substr($1,3,1) != "C" || $5 == "PME" ) print $0 }' archivo.txt

#Uso de if con else
awk 'BEGIN {FS="|"}; { if( $1 == condicion ) { print $2 } else { printf  $1 $2 } }' archivo.txt


#Reemplaza valor "|" por ";"
awk '{ gsub(/\|/, ";");print }' archivo.txt


#Remplazar o eliminar un valor de un archivo
awk '{ sub(/20090327/, "");print }' archivo.txt


#Usar una variable definida anteriormente 
Fecha=20110903
awk 'BEGIN {FS="|"}; { print $2 VAR}' VAR=$Fecha archivo.txt
 
#Cambia en el campo 1 cuando la palabra es 'Antes' por la palabra 'Despues'
awk 'BEGIN {FS="|"}; {if ( $1 == "Antes" ) {($1="Depues")}; {OFS="|"}; print $0 }' archivo.txt
#FS="|": indica separador de campo del archivo
#OFS="|": indica separador de salida

#Sumar toda una columna de un archivo con AWK
awk 'BEGIN {FS=";"}; ($3 == "N") { sum += $1 } END { print sum }' archivo.txt

#Sumar con una condicion (agrupar datos) con AWK
awk 'BEGIN {FS=";"}; {if ($3 == "N") { sum += $1 }} END { print sum }' archivo.txt

#remplaza en la columna 4, el "/" por " y "
awk 'BEGIN {FS=";"}; {gsub("/"," y ",$4); print $4 ":"}' archivo.txt





############################### 
#COMADOS UNIX 
###############################










#Busca palabra en archivo.txt 
grep -i "palabra" archivo.txt
#-c 	Numero de filas con match
#-i 	Realiza match ignorando mayúsculas y minúsculas
#-n 	Imprime numero de línea y Filas con match 
#-v 	Todas las filas que no hacen Match



#copiar una archivo consecutivo de otro (siendo -d el separador, por defecto es Tab)
paste -d '|' archivo1.txt archivo2.txt


#ordenar archivos
cut -d : f1 Ejemplos2.txt | sort








#obtiene numero de líneas de un archivo
Num_Lineas=`wc -l archivo.txt | awk '{ printf "%i", $1 }'`


#obtiene numero de lines de un archivo comprimido
zcat supd00.txt.Z|wc -l

#De un archivo comprimido, extraer una parte de este descomprimido.
zcat supd00.txt.Z|head -50000

#CORTAR ARCHIVOS
 #corta de un archivo la columna 2 hasta la 4 con separador punto coma (;)
cut -f 2-4 -d";" archivo.txt
#-f Columnas a cortar
#-d Separador del archivo
#-s Indica que las líneas que no posean el delimitador (o separador) no sean mostradas.

#Join de dos archivos
#Primero se debe ordenar los archivos por la columna que se van a unir

sort -t ';' -k 2,3 archivo1.txt >  archivo1_sort.txt
sort -t ';' -k 1,1 archivo2.txt >  archivo2_sort.txt

#Siendo -K indicador desde y hasta que columna se va a ordenar


join -t ';' -j1 2 -j2 1 archivo1_sort.txt archivo2_sort.txt > resultado.txt
# -t: separador
# -j1 2 -j2 1: indica join entre archivo1_sort.txt  columna 2 contra archivo1_sort.txt 2 columna 1
# -a1: indica left join al archivo1_sort.txt
# -a2: indica right join al archivo2_sort.txt
# -e: si no cruza coloca un valor por defecto (ejemplo: -e N)
# -o: indica campos que iran en el archivo de salida (NumArch.NumColmn) 
# -v: Todas las filas que no hacen Match de uno de los archivos: ej -v 1
#	  ejemplo: 1.1 2.1   (indica archivo 1 columna 1 y archivo 2 columna 1)


#Transforma de Minuscula a Mayuscula
var=hola
VarEnMayus=`echo $var | tr '[:lower:]' '[:upper:]'` 

echo $VarEnMayus #(resultado: HOLA)


#Obtener los n primeros registros de un archivo
head -100 archivo.txt


#Obtener los n ultimos registros de un archivo
tail -100 archivo.txt


#Comprimir archivo
compress archivo.txt


#Descomprimir archivo
uncompress archivo.txt


#Uso del Nohup
nohup sh nombre_shell.sh parametro_1 paramtero_n &


#Realizar operaciones aritmeticas (resultado 15)
var1=10
var2=5
echo $(($var1+$var2))



















#Realiza substring a una variable (resultado: 01)
periodo=200801
mes=`expr substr $periodo 5 6`

#dar permisos a un archivo
chmod 777 archivo.txt


#valida que archivo exista
if [ ! -f archivo.txt ]
then
echo "No existe"
fi

#valida solo numeros
echo $1 | grep "[a-zA-Z]"
if test $? -lt 1

fi

#muestra tamaño del archivo
du -k archivo.txt







#compara 2 archivos y arroja las lineas diferentes
diff archivo1.txt archivo2.txt > campos_diferentes.txt
#------------------------------------------------
#diff -w [fichero1] [fichero2] = Descarta el espacio en blanco cuando compara las lineas
#diff -q [fichero1] [fichero2] = Informa solo si los ficheros son distintos
#diff -y [fichero1] [fichero2] = Muestra la salida a dos columnas



# calculadora de unix 'bc'
raiz cuadrada: sqrt(numero)


 
#cuenta cantidad de registros diferentes
sort "Columna_5.txt" | uniq | wc -l

#Elimina Encabezado y pie
nreg=`wc -l $DirTmp/Mat_Ind.txt|awk '{print $1}'`
nreg="`expr ${nreg} - 1`"
head -n ${nreg} $DirTmp/Mat_Ind.txt|tail +3 > $DirTmp/Mat_Ind2.txt
Eliminar_Archivo $DirTmp/Mat_Ind.txt


#elimina espacios al final de una columna
awk '{gsub(/[ \t]+$/, "", $2); print $2 ":"}'

#elimina espacios al principio de una columna
awk '{gsub(/^[ \t]+/, "", $2); print $2 ":"}'

#elimina espacios al final y al comienzo de una columna
awk '{gsub(/^[ \t]+/, "", $2); gsub(/[ \t]+$/, "", $2); print $2 ":"}'

#Muestra un cartel ascii con el texto de forma vertical
banner "texto"

#-------
#isql
#-------



#enlista todas las tablas de una base de datos
#type='V' para vistas
#type='P' procedimientos
isql -Ufirmpod -Pfirmpod1 << eof > tablas.txt
use firmpod
go
select name from sysobjects where type='U'
go
eof






#elimina todos los archivos que coinsidan con el nombre, donde su fecha de modificacion + $cantidadDias, sea menor que la fecha actual
find $DirBkp/${1}_ayer_{*}.csv.Z -mtime +${cantidadDias} -exec rm -f {} \; 2>/dev/null



#obtiene el hash de una conjunto de columnas (el hash es un numero unico para un conjunto mde caracteres)
select hashbytes('ptn', columna1, columna2) from tabla























#cambia la fecha de modificacion de un fichero (AAAAMMDDhhmm.ss)
touch -t 201606221359.25 archivo.txt
















#Realizar operaciones aritmeticas (resultado 15)
var1=10
var2=5
echo $(($var1+$var2))
#O
resultado=`expr $var1 + $var2`
#O

#Calculadora de unix
echo $var1 + $var2 | bc 



#Raiz cuadrada, retorna valor 3
bc
sqrt(9)
3




#Generacion de listado de archivo
find ./ > $HOME/rev_list_archivo.txt

# Obtencion hash de archivo
find ./ > $HOME/rev_list_archivo_prod.txt
while read FILE; do echo $(cksum "$FILE") >> $HOME/rev_list_archivo_md5_prod.txt;done < $HOME/rev_list_archivo_prod.txt

find ./ > $HOME/rev_list_archivo.txt
while read FILE; do echo $(cksum "$FILE") >> $HOME/rev_list_archivo_md5.txt;done < $HOME/rev_list_archivo.txt

#Renombrado masivo
CONT=1;for FILE in *.hoy;do cp "$FILE" .."$CONT-20160920-poderes-full-B-stock.ayer.carga-inicial";CONT=$(($CONT+1));done

#Imprimir líneas de 3000 caracteres o más:    
sed -n '/^.\{3000\}/p' fichero      
#Imprimir líneas de 3000 caracteres o menos:  
sed -n '/^.\{3000\}/!p' fichero














